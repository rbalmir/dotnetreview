﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareReview
{
  struct ElementValueType
  {
    private int t;

    public ElementValueType(int value){
      this.t = value;
    }

    public void SetValue(int value)
    {
      t = value; 
    }

    public int GetValue()
    {
      return t;
    }

    public House GetHouse()
    {
      return new House();
    }

  }

 
  class ElementRefType 
  {
    private int t;

    public ElementRefType(int value)
    {
      this.t = value;
    }

    public void SetValue(int value)
    {
      t = value;
    }

    public int GetValue()
    {
      return t;
    }
  }

}
