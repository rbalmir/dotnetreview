﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareReview
{
  internal class House
  {
    protected int windowCount = 0;   

    int WINDOWCOUNT { 
      get { return windowCount;       } 
      set { this.windowCount = value; } 
    }

    ~House()
    {
      Console.WriteLine("Unmanaged Resource removed!");
    }

  }

}
