﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace SoftwareReview
{
  class LearningStructs
  {

    static public void UnderstandingStructs()
    {

      //Boxing primitive
      Object o = 9;
      int x = (int)o;
      Console.WriteLine(o);

      Console.WriteLine("UNDERSTANDING STRUCTS");

      //Value Type : 
      ElementValueType e1 = new ElementValueType(1);
      ElementValueType e2 = e1;

      Console.WriteLine(e1.GetHouse());

      Console.WriteLine("STRUCTS");
      PrintGCAddress(e1);
      Console.WriteLine(String.Format("#", 10));
      Console.WriteLine("NOTE - Assign variable to another variable - does make a copy but generates same hashcode");
      Console.WriteLine("Element One:{0} HashCode:{1}", e1.ToString(), e1.GetHashCode());
      Console.WriteLine("Element Two:{0} HashCode:{1}", e2.ToString(), e2.GetHashCode());

      Console.WriteLine("NOTE - When the property of assigned variable is changed that when the runtime creates the copy");
      e2.SetValue(2);
      Console.WriteLine("Element One:{0} HashCode:{1} Value:{2}", e1.ToString(), e1.GetHashCode(), e1.GetValue());
      Console.WriteLine("Element Two:{0} HashCode:{1} Value:{2}", e2.ToString(), e2.GetHashCode(), e2.GetValue());

      //The compilier appears to pass this by reference for performance purpose.
      Console.WriteLine("NOTE - When passing Element One by value");
      LearningStructs.PassingStruct(e1);

      Console.WriteLine("\n");

      //Class : Reference Type
      ElementRefType e3 = new ElementRefType(1);
      ElementRefType e4 = e3;

      Console.WriteLine("ClASS : Have value type sematics! ");
      PrintGCAddress(e3);
      Console.WriteLine("Element One:{0} HashCode:{1}", e3.ToString(), e3.GetHashCode());
      Console.WriteLine("Element Two:{0} HashCode:{1}", e4.ToString(), e4.GetHashCode());

      e4.SetValue(2);

      Console.WriteLine("Element One:{0} HashCode:{1} Value:{2}", e3.ToString(), e3.GetHashCode(), e3.GetValue());
      Console.WriteLine("Element Two:{0} HashCode:{1} Value:{2}", e4.ToString(), e4.GetHashCode(), e4.GetValue());
    }

    static public void PassingStruct(ElementValueType e)
    {
      Console.WriteLine("Element One Passed By Value:{0} HashCode:{1}", e.ToString(), e.GetHashCode());
    }

    //Attempting to view memory VM location
    static public void PrintGCAddress(Object obj)
    {
      GCHandle objHandle = GCHandle.Alloc(obj, GCHandleType.WeakTrackResurrection);
      int a = GCHandle.ToIntPtr(objHandle).ToInt32();
      Console.WriteLine("Mem Address:", a);
    }



  }
}
